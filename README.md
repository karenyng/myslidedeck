#Prerequisites 
* download and install [reveal.js](https://github.com/hakimel/reveal.js/) 

## create content from markdown
```$ create_js_from_md.sh ${OUTPUT.html} ${INPUT.md}```
* execute - this script specifies path to markdown file and
the output file   

## set up a template for the slides  
* template.html - for now, see `create_js_from_md.sh` for details 

## slide usage / debugging 
```$ grunt serve``` 
* will allow the display of "index.html" from a local server
* speaker notes - press 's' on your keyboard


#!/bin/bash

pandoc --section-divs -t html5 -s --incremental --slide-level 1 --template template.html\
 --mathjax -o ${1} ${2}
#~/Documents/Research/slides/groupmeeting_apr14/presentation.md
